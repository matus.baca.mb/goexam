package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
)

func main() {
	res, err := http.Get("https://openlibrary.org/authors/OL45865W.yml")
	if err != nil {
		log.Fatal(err)
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		log.Fatal(err)
	}

	var response Response
	json.Unmarshal(body, &response)

	for i, p := range response.Persons {
		fmt.Println("Authors works", (i + 1), ":", "https://openlibrary.org/authors/", p.Author.Key)
		fmt.Println("----------------")
	}
}

type Response struct {
	Persons []Person `json:"authors"`
}

type Person struct {
	Author Author `json:"author"`
}

type Author struct {
	Key string
}
